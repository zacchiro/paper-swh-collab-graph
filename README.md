Paper - Software Heritage Collaboration Graph
=============================================

Paper about the analysis of the [Software Heritage][1] collaboration graph.

[1]: https://www.softwareheritage.org


Scratch pad / brainstorming
---------------------------

A separate Etherpad document used for brainstorming and collaborative note taking is available at <https://pad.sfconservancy.org/p/zYIHQzBbw91V40hrKpR9> .
