import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.lang.NumberFormatException;

/**
 * read the temporal Software Heritage graph in order to extract largest strike of years 
 * for contributors or repositiries.
 * usage : zstdcat '2022-12-07/contribution_graph.years_buckets.csv.zst' | java SHLongestStrike -Xmx 20G
 *
 * @author Fabien de Montgolfier
 */

class VectAnnee {
    /* a class for a bitmap of years. true if a contribution for the given year. */
    public static final int nb_annees = 80;
    boolean[] annee;
    int nb; // number of true, ie, number of contributed years 
    int strike; // size of longuest strike, ie, consectuve true

    VectAnnee() {
	annee = new boolean[nb_annees];
	nb=0;
	strike=0;
    }

    void setAnnee(int an) {
	// put a true in the bitmap, and updates nb and strike
	an = 2023-an; // parameter is a year. normalized between 2023 and 2023-nb_annees
	if(annee[an]==false) {
	    annee[an]=true;
	    nb++;
	    int current_strike=1;
	    int i = an-1;
	    while(i>=0 && annee[i--]==true)
		current_strike++;
	    i=an+1;
	     while(i<nb_annees && annee[i++]==true)
		 current_strike++;
	     if(current_strike>strike)
		 strike = current_strike;
	}
    }
	      
}

 
public class SHLongestStrike {
    
    public static final int nbcontributors = 120000000; // estimated number of contributor
    public static final int nbrepos = 1600000000; // and of repositories
   
    public static void main(String args[]) {
	// the first hashmap associates a VactAnnee to each contributor id
	Hashtable <Integer,VectAnnee> contrib = new	Hashtable<>(nbcontributors);
	// and the second, to each repository id
	Hashtable <Long,VectAnnee> repos = new	Hashtable<>(nbrepos);
	int id=0, annee=0;
	long repo = 0;
	// best strike (consecutive year) and sum (total years contributed) for an user
	int beststrikeid=0, beststrike=0, bestsumid=0, bestsum=0;
	//idem for a repository
	long beststrikerepo=0, beststrike2=0, bestsumrepo=0, bestsum2=0;
	long linenum=0;
	try {
	    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
	    reader.readLine(); // skip first line
	    String line = reader.readLine();
	    while (line != null) {
		int i=0; // i scans line
		int level = 0;
		// level 0 : reading repo. level 1 : reading contrib. level >= 2 : reading year
		for(i=0; i<line.length();i++)
		    {
			char c = line.charAt(i);
			if(c>='0' && c<= '9') {
			    if(level==0)
				repo = 10*repo + c -'0';
			    else if (level==1)
				id = 10*id + c-'0';	
			    else
				annee = 10*annee + c -'0';
			}
			else if(c!=',' && c!=' ')
			    System.out.println("Something wrong line "+linenum+" "+line);
			
			if(i==line.length()-1 || c==',' || c==' ') {// finished a parsing
			    
			    level++;
			    //System.out.println("OK !! repo ="+repo+" id= "+id+" year ="+annee+" level= "+level+" line="+line);
			    if(level >=3) // read repo, id and year
				{
				    if(!contrib.containsKey(id))
					contrib.put(id, new VectAnnee());
				    
				    VectAnnee va1 = contrib.get(id);
				    
				    va1.setAnnee(annee);
				    if(va1.nb>bestsum)
					{
					    System.out.println("new best sum "+va1.nb+" for id "+id);
					    bestsumid = id;
					    bestsum = va1.nb;
					}
				    if(va1.strike>beststrike)
					{
					    System.out.println("new best strike "+va1.strike+" for id "+id);
					    beststrikeid = id;
					    beststrike = va1.strike;
					}
				    
				    // same job for the repo id hashmap. 
				    if(!repos.containsKey(repo))
					repos.put(repo, new VectAnnee());
				    
				    VectAnnee va2 = repos.get(repo);
				    
				    va2.setAnnee(annee);
				    if(va2.nb>bestsum2)
					{
					    System.out.println("new best sum "+va2.nb+" for repo "+repo);
					    bestsumrepo = repo;
					    bestsum2 = va2.nb;
					}
				    if(va2.strike>beststrike2)
					{
					    System.out.println("new best strike "+va2.strike+" for repo "+repo);
					    beststrikerepo = repo;
					    beststrike2 = va2.strike;
					}
				}
	     
			    if(c==' ') // year separator
				annee=0; // reset annee for next year
			    else if(c!=',') // end of line
				{
				    id=0;
				    repo=0;
				    annee=0;
				}
			}
			
		    }
		// read next line, stop if null. progress indication every then million lines read.
		line = reader.readLine();
		linenum++;
		if(linenum%10000000==0)
		    System.out.println("readline line "+linenum+" : "+line);
	    
	    }
	}catch (IOException e) {
	    e.printStackTrace();
	}

    }
    
}
