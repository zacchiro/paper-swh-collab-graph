# Depends: latexmk

MAIN_tex = main.tex
MAIN_pdf = $(patsubst %.tex,%.pdf,$(MAIN_tex))

MAIN_tex_full = main--expanded.tex
DEPS = $(filter-out $(MAIN_tex_full),$(wildcard *.tex *.bib))

all: $(MAIN_pdf)

$(MAIN_pdf): $(DEPS)
	latexmk $(MAIN_tex)

$(MAIN_tex_full): $(DEPS)
	latexpand $(MAIN_tex) > $@

.PHONY: wc
wc: $(MAIN_tex_full)
	texcount $<

.PHONY: clean
clean:
	latexmk -c $(MAIN_tex)

.PHONY: distclean
distclean:
	latexmk -C $(MAIN_tex)
